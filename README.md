






Tales of Numbers - introductory math and computing textbook

**(in-progress; nowhere near complete)**

<br>

[comment]: # (Section header: Quick links) 

[comment]: # (Link: Book PDF)

[comment]: # (Link: Code labs repo)


# Quick links

- **Book [PDF](https://www.dropbox.com/scl/fi/dm2yjnw59t76hyelo05cb/tales-math-book-20240608-30fa5205090928.pdf?rlkey=a37l13xsfryx6kr95h90svtmb&st=r58miyxs&dl=0)**

- **Code lab [repo](https://gitlab.com/kyuh2/tales-math-book-code-examples)**

<br>

# Summary

The goal of this project is to be a good introduction to math. If someone says, "introduce math to me and give me a reasonably full story", this project hopes to be a good answer.



About this project:

- Gives a "short but rigorous" tour of many math topics  (starting from arithmetic and touching on properties of numbers, proofs/logic, introductory algebra, calculus, precalculus, differential equations, linear algebra, complex analysis, computational aspects, and more)

- Gets into (hopefully) interesting problems, patterns, and observations

- Shows computational "labs" corresponding to the math (code included)

- Ultimately is a "tale of numbers".  What *are* the numbers and why are they special?









<br>

# Structure

This book will be approximately 20 chapters. Each chapter generally has:

- **Main chapter text**

- **Summary** of the chapter

- **Exercises**  (within the main text)

- **Problems**  (after the main text)

- **Final problem** that culminates the chapter

- **Hints** for the problems

- **Solutions** for the problems



Alongside the book itself are:

- **Labs** that illustrate the math, and introduce some programming -- includes demos, simulations, and more.  These are implemented in python and C++, and can be found [here](https://gitlab.com/kyuh2/tales-math-book-code-examples).

- **Further reading** -- books, Wikipedia, practice problems, other online links


<!--

<br>

### Formatting

TODO
-->
[comment]: # (Todo: green/blue/lilac and other style guide)

<br>


[comment]: # (T:  Sometimes it's helpful to illustrate math concepts by using a machine that can do billions of simple math ops per second)

[comment]: # (T:  You can learn most of this stuff online)

<!--
### Levels of detail-->

<!--From highest-to-lowest: -->

<!--1. Chapters themselves-->
<!--2. End-of-chapter summaries-->
<!--3. Detailed outline (TODO)-->
<!--4. Chapter outline (below)-->

<br>






# Chapter outline



#### Chapter 1: "Counting"

- **High-level:**  "How do we count things?"

- **Topics:**  Numbers for counting (0, 1, 2, ...), basic ops of these numbers, induction, counting various things, "powers of sums" (binomial expansion)

- **Final problem**:  "Count the 3s"

[comment]: # (**Selected reading**)

<br>


#### Chapter 2: "Uncovering more numbers"

- **High-level:**  "What other numbers are there?"

- **Topics:**  Logical statements, divisibility, prime numbers, prime factorization (fundamental theorem of arithmetic), fractions, >>square root of 2<< irrational numbers, negative numbers, computer representation of numbers

- **Final problem**:  1988 IMO #6 [^imoproblems]

[comment]: # (**Selected reading**)

<br>

#### Chapter 3: "Finite and infinite things"

- **High-level:**  Some combination of: 
	- "how can we describe infinity"
	- "what are functions"
	- "what are the real numbers"

- **Topics:**
	- Mathematical sets, data structures, least upper bounds, >>square root of 2 again<<, what are real numbers, functions in math, functions in computing, <br>
	- (More on functions in math -- domain/range/codomain, injective/surjective/bijective, composition, inverse, "multiple-input-functions")
	- Infinite sequences, limit of a sequence, monotone convergence theorem, calculating square roots, >>square root of 2 again<<, >>what are real numbers again<<, special infinite sequences, repeating decimals (again), polynomial and rational sequences, asymptotic growth (big-Oh)

- **Final problem:**
	- 2014 IMO #1  [^imoproblems]
	- 2022 IMO #2  [^imoproblems]
	- 1985 IMO #6  [^imoproblems]  (part 1)



<br>

#### Chapter 4: Continuity

- **High-level:**

- **Topics:** 
	- Polynomial and rational functions (over rationals/reals), delta-epsilon limits, continuity, continuous functions, intermediate value theorem, the real numbers (again)

- **Final problem:**
	- 1985 IMO #6  [^imoproblems]  (part 2)
	- TBD

<br>



#### Chapter 5: Accumulation (integrals chapter)

- **Topics:** Units of measurement, area under a function, the integral, integrable functions

- **"Final" problem:** Integral from (a->b) + (b->c) = (a->c)  -- halfway through the chapter



<br>

#### Chapter 6: Changes (derivatives chapter)

- **High-level:** How do we describe change?









<br>

<br>

[^imoproblems]: (Footnote) These are problems from the International Mathematical Olympiad (IMO) and can be found on their official website [here](https://www.imo-official.org/problems.aspx)




